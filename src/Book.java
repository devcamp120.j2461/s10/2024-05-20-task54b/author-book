public class Book {
    private String name;
    private Double price;
    private Author author;
    private Integer qty = 0;
    public Book(String name, Double price, Author author, Integer qty) {
        this.name = name;
        this.price = price;
        this.author = author;
        this.qty = qty;
    }
    
    public Book(String name, Double price, Author author) {
        this.name = name;
        this.price = price;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "Book [name=" + name + ", price=" + price + ", author=" + author.toString() + ", qty=" + qty + "]";
    }
}
