public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Author author1 = new Author("Alex","alex@gmail.com",'m');
        Author author2 = new Author("Peter", "peter@gmail.com", 'm');

        System.out.println(author1);
        System.out.println(author2);

        Book book1 = new Book("The rabbit", 15.00, author1);
        Book book2 = new Book("Seven Oceans", 20.00, author2, 5);

        System.out.println(book1);
        System.out.println(book2);
    }
}
